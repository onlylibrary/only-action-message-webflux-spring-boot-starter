package com.onlyxiahui.framework.action.dispatcher.general.constant;

/**
 * 
 * <br>
 * Date 2019-12-03 16:17:57<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public enum HeadProperty {

	/**
	 * 消息的id，标识消息的唯一性
	 */
	key("key"),
	/**
	 * 请求接口名称
	 */
	service("service"),
	/**
	 * 请求动作类型
	 */
	action("action"),
	/**
	 * 请求方法
	 */
	method("method"),
	/**
	 * 请求接口版本
	 */
	version("version"),
	/**
	 * 响应时间
	 */
	timestamp("timestamp");

	String name;

	HeadProperty(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
