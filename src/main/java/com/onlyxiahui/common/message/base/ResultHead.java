package com.onlyxiahui.common.message.base;

import java.util.HashMap;

import com.onlyxiahui.common.message.node.Head;

/**
 * 
 * <br>
 * Date 2019-09-06 15:51:58<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ResultHead extends HashMap<String, Object> implements Head {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 消息的id，标识消息的唯一性
	 */
	private String key;
	/**
	 * 请求接口名称
	 */
	private String service;
	/**
	 * 请求动作类型
	 */
	private String action;
	/**
	 * 请求方法
	 */
	private String method;
	/**
	 * 请求接口版本
	 */
	private String version;
	/**
	 * 响应时间
	 */
	private Long timestamp;

	@Override
	public String getKey() {
		key = this.getValue("key", String.class);
		return key;
	}

	@Override
	public void setKey(String key) {
		this.key = key;
		this.put("key", key);
	}

	public String getService() {
		service = this.getValue("service", String.class);
		return service;
	}

	public void setService(String service) {
		this.service = service;
		this.put("service", service);
	}

	public String getAction() {
		action = this.getValue("action", String.class);
		return action;
	}

	public void setAction(String action) {
		this.action = action;
		this.put("action", action);
	}

	public String getMethod() {
		method = this.getValue("method", String.class);
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
		this.put("method", method);
	}

	public String getVersion() {
		version = this.getValue("version", String.class);
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
		this.put("version", version);
	}

	public Long getTimestamp() {
		timestamp = this.getValue("timestamp", Long.class);
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
		this.put("timestamp", timestamp);
	}

	@SuppressWarnings("unchecked")
	public <T> T getValue(String key, Class<T> calzz) {
		Object o = this.get(key);
		return calzz.isAssignableFrom(calzz) ? (T) o : null;
	}
}
