package com.onlyxiahui.extend.spring.webflux.dispatcher.message.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.onlyxiahui.framework.action.dispatcher.general.extend.impl.ActionMessageResolverImpl;
import com.onlyxiahui.framework.action.dispatcher.general.extend.impl.ResponseBodyHandlerImpl;
import com.onlyxiahui.framework.action.dispatcher.general.extend.impl.ResultHandlerImpl;

/**
 * <br>
 * Date 2019-12-03 17:32:35<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Configuration
@Import({
		ActionMessageResolverImpl.class,
		ResponseBodyHandlerImpl.class,
		ResultHandlerImpl.class
})
public class OnlyDispatcherHandlerConfig {

}
